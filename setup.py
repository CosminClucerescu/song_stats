"""Install the song_stats web app."""
from setuptools import setup

setup(
    setup_requires=['pbr'],
    pbr=True,
)
