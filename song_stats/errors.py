from flask import jsonify

from song_stats.flask_app import song_stats_app


class BaseError(Exception):
    status_code = 400

    @property
    def message(self):
        raise NotImplementedError()

    def __init__(self, payload=None):
        self.payload = payload

    def to_dict(self):
        ret_val = dict(self.payload or {})
        ret_val['message'] = self.message
        return ret_val


class InvalidRating(BaseError):
    status_code = 400
    message = "Rating must be in range {}, {}"

    def __init__(self, min_rating, max_rating):
        super(InvalidRating, self).__init__()
        self.message = self.message.format(min_rating, max_rating)


@song_stats_app.errorhandler(BaseError)
def handle_invalid_rating(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response
