from flask import send_file

from song_stats.flask_app import song_stats_app


# TODO: Should become a separate project, only the REST api should be kept in this project.
@song_stats_app.route('/show_stats', methods=['GET'])
def show_entries_html():
    return send_file('static/entries.html')
