import os
from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy

song_stats_app = Flask(__name__)
song_stats_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
song_stats_app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///ssPlayerDb.sqlite3'
if 'SQLITE_DB_PATH' in os.environ:
    song_stats_app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.environ['SQLITE_DB_PATH']

api = Api(song_stats_app)
song_stats_db = SQLAlchemy(song_stats_app)
