from sqlalchemy import func, and_

from song_stats.flask_app import song_stats_db
from song_stats.models.play_count import PlayCount
from song_stats.models.rating import Rating
from song_stats.errors import InvalidRating


class Song(song_stats_db.Model):
    __table_args__ = (
        song_stats_db.UniqueConstraint("artist", "title"),
    )
    id = song_stats_db.Column(song_stats_db.Integer, primary_key=True)
    artist = song_stats_db.Column(song_stats_db.String(256), nullable=False)
    title = song_stats_db.Column(song_stats_db.String(256), nullable=False)
    play_counts = song_stats_db.relationship('PlayCount', backref='song', lazy='dynamic')
    ratings = song_stats_db.relationship('Rating', backref='song', lazy='dynamic')

    def __repr__(self):
        return '<Song Stats for  {}-{}:\nPlayCount: {}, Rating: {}>'.format(self.artist, self.title, self.play_count,
                                                                            self.rating)

    def to_dict(self):
        """

        :return:
        """
        return {
            "artist": self.artist,
            "title": self.title,
            "play_count": self.play_count,
            "rating": self.rating
        }

    @property
    def play_count(self):
        """

        :return:
        """
        return self.play_counts.count()

    @property
    def rating(self):
        """

        :return:
        """
        last_rating = self.ratings.order_by(Rating.id.desc()).first()
        return None if not last_rating else last_rating.rating

    @classmethod
    def _get_or_create(cls, artist, title):
        """

        :param artist:
        :param title:
        :return:
        """
        artist = artist.lower().strip()
        title = title.lower().strip()
        song = Song.query.filter(and_(func.lower(Song.artist) == artist, func.lower(Song.title) == title)).first()
        if not song:
            song = Song(artist=artist, title=title)
            song_stats_db.session.add(song)
            song_stats_db.session.commit()
        return song

    @classmethod
    def rate_id(cls, song_id, rating):
        if rating > 10 or rating < 0:
            raise InvalidRating(0, 10)
        song_stats_db.session.add(Rating(rating=rating, song_id=song_id))
        song_stats_db.session.commit()

    @classmethod
    def rate(cls, artist, title, rating):
        song = cls._get_or_create(artist, title)
        cls.rate_id(song.id, rating)

    @classmethod
    def increase_play_count(cls, artist, title):
        song = cls._get_or_create(artist, title)
        song_stats_db.session.add(PlayCount(song_id=song.id))
        song_stats_db.session.commit()
