import datetime

from song_stats.flask_app import song_stats_db


class PlayCount(song_stats_db.Model):
    id = song_stats_db.Column(song_stats_db.Integer, primary_key=True)
    date = song_stats_db.Column(song_stats_db.DateTime, default=datetime.datetime.utcnow)
    song_id = song_stats_db.Column(song_stats_db.Integer, song_stats_db.ForeignKey("song.id"), nullable=False)
