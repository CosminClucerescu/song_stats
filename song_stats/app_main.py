"""

"""
from song_stats.flask_app import song_stats_app, song_stats_db
from song_stats.models.song import Song
from song_stats import rest
from song_stats import pages

song_stats_db.create_all()


def main():
    """
    Entrypoint
    :return:
    """
    song_stats_app.run(host='0.0.0.0', port=5002)


if __name__ == "__main__":
    main()

