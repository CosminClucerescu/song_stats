from song_stats.models.rating import Rating

from song_stats.models.play_count import PlayCount
from song_stats.models.song import Song
from flask import jsonify, request
from flask_restful import abort
from song_stats.flask_app import song_stats_app


@song_stats_app.route('/rate', methods=['POST'])
def rate():
    if not request.json or 'artist' not in request.json or 'title' not in request.json or 'rating' not in request.json:
        abort(400)
    Song.rate(request.json['artist'], request.json['title'], int(request.json['rating']))
    return jsonify({}), 201


@song_stats_app.route('/increase_play_count', methods=['POST'])
def increase_play_count():
    if not request.json or 'artist' not in request.json or 'title' not in request.json:
        abort(400)
    Song.increase_play_count(request.json['artist'], request.json['title'])
    return jsonify({}), 201


def _get_last_played():
    return Song.query.join(PlayCount).filter().order_by(PlayCount.id.desc()).first()


@song_stats_app.route('/last_rated', methods=['GET'])
def get_last_rated():
    song = Song.query.join(Rating).filter().order_by(Rating.id.desc()).first()
    if not song:
        return jsonify({})
    return jsonify(song.to_dict())


@song_stats_app.route('/last_played', methods=['GET'])
def get_last_played():
    song = _get_last_played()
    if not song:
        return jsonify({})
    return jsonify(song.to_dict())


@song_stats_app.route('/rate_last_played', methods=['POST'])
def rate_last_played():
    if 'rating' not in request.json:
        abort(400)
    song = _get_last_played()
    if not song:
        abort(400)
    rating = int(request.json['rating'])
    if song.rating != rating:
        Song.rate_id(song.id, rating)
    return jsonify({'artist': song.artist,
                    'title': song.title}), 201


@song_stats_app.route('/', methods=['GET'])
def show_entries():
    entries = Song.query.all()
    return jsonify([entry.to_dict() for entry in entries])
